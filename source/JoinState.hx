package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.addons.ui.FlxUIInputText;
import PlayState;

class JoinState extends FlxState
{
	var _joinButton: FlxButton;
	var _nameField: FlxUIInputText;
	var _joinLabel: FlxText;
	override public function create():Void
	{
		this._joinButton = new FlxButton(0, 0, "Join", function() {
			trace("Changing state from JoinState to Playstate");
			FlxG.switchState(new PlayState());
		});

		this._joinButton.screenCenter();
		this._nameField = new FlxUIInputText();
		this._nameField.screenCenter();
		this._nameField.y -= 25;
		this._joinLabel = new FlxText(0, 0, 0, "Please set your nickname");
		this._joinLabel.screenCenter();
		this._joinLabel.y -= 50;
		this.add(this._joinButton);
		this.add(this._nameField);
		this.add(this._joinLabel);

		var button = new FlxButton(0, 0, "Fullscreen", function() FlxG.fullscreen = !FlxG.fullscreen);
		add(button);
		super.create();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
}
