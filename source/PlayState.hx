package;

import flixel.util.FlxSpriteUtil;
import flixel.FlxG;
import flixel.tile.FlxTilemap;
import flixel.FlxState;

class PlayState extends FlxState
{
	static var TILESIZE: Int = 16;
	static var WIDTHTILES: Int = 0;
	static var HEIGHTTILES: Int = 0;
	var _map: FlxTilemap;
	var _player: Player;
	
	override public function create():Void
	{
		WIDTHTILES = Std.int(FlxG.width / TILESIZE);
		HEIGHTTILES = Std.int(FlxG.height / TILESIZE);
		super.create();
		_map = new FlxTilemap();
		var total: Int = Std.int(FlxG.width / TILESIZE) * Std.int(FlxG.height / TILESIZE);
		trace('$WIDTHTILES x $HEIGHTTILES = $total tiles');
		var mapArray: Array<Int> = [
			for(y in 0...HEIGHTTILES)
				for(x in 0...WIDTHTILES)
					y == 0 || y == HEIGHTTILES -1 ? 1 : 0
		];
		_map.loadMapFromArray(mapArray, Std.int(FlxG.width / TILESIZE), Std.int(FlxG.height / TILESIZE), "assets/images/game_tiles.png", 16, 16);
		add(_map);
		_player = new Player(32, 32);
		add(_player);
	}

	override public function update(elapsed:Float):Void
	{
		// FlxSpriteUtil.screenWrap(_player, true, true);
		FlxG.collide(_player, _map);
		super.update(elapsed);
	}
}
