package;

import flixel.util.FlxSpriteUtil;
import flixel.input.actions.FlxAction.FlxActionDigital;
import flixel.FlxG;
import flixel.ui.FlxButton;
import flixel.input.actions.FlxActionManager;
import flixel.util.FlxColor;
import flixel.FlxSprite;

class Player extends FlxSprite
{
    var _actionManager: FlxActionManager;
    var _up: FlxActionDigital;
	var _down: FlxActionDigital;
	var _left: FlxActionDigital;
	var _right: FlxActionDigital;
	var _shoot: FlxActionDigital;
    var _jump: FlxActionDigital;
    
    var _vpad: VirtualJoystick;
    public function new(?X:Float=0, ?Y:Float=0)
    {
        super(X, Y);
        _actionManager = new FlxActionManager();
        FlxG.inputs.add(_actionManager);
        makeGraphic(16, 16);
        setSize(14, 14);
        offset.set(1, 1);
        // Physics
        drag.x = 640;
        acceleration.y = 420;
        maxVelocity.set(120, 200);

        _up = new FlxActionDigital("_up").addKey(UP, PRESSED).addKey(W, PRESSED);
        _down = new FlxActionDigital("_down").addKey(DOWN, PRESSED).addKey(S, PRESSED);
        _left = new FlxActionDigital("_left").addKey(LEFT, PRESSED).addKey(A, PRESSED);
        _right = new FlxActionDigital("_right").addKey(RIGHT, PRESSED).addKey(D, PRESSED);
        
        #if VIRTUAL_PAD
        trace("Virtual Pad enabled");
        _vpad = new VirtualJoystick();
        _up.addInput(_vpad.buttonUp, PRESSED);
        _down.addInput(_vpad.buttonDown, PRESSED);
        _left.addInput(_vpad.buttonLeft, PRESSED);
        _right.addInput(_vpad.buttonRight, PRESSED);
        FlxG.state.add(_vpad);
		#end
        _actionManager.addActions([_up, _down, _left, _right]);

        
    }
    public function updateActions() {
        if(_right.triggered) {
            acceleration.x += drag.x;
        } 
        if(_left.triggered) {
            acceleration.x -= drag.x;
        }
        if(_up.triggered) {
            if(this.velocity.y == 0) {
                this.y -= 1;
                this.velocity.y = -200;
            }        
        }
    }

    override public function update(elapsed:Float) {
        acceleration.x = 0;
        FlxSpriteUtil.screenWrap(this);
        updateActions();
        super.update(elapsed);
    }
}