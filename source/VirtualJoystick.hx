package;

import flixel.FlxG;
import flixel.graphics.frames.FlxTileFrames;
import flixel.group.FlxSpriteGroup;
import flixel.math.FlxPoint;
import flixel.system.FlxAssets;
import flixel.util.FlxDestroyUtil;
import flixel.FlxSprite;
import flixel.ui.FlxButton;

/**
 * A gamepad which contains 4 directional buttons and 4 action buttons.
 * It's easy to set the callbacks and to customize the layout.
 *
 * @author Ka Wing Chin
 */
class VirtualJoystick extends FlxSpriteGroup
{
	public var buttonA:FlxButton;
	public var buttonB:FlxButton;
	public var buttonLeft:FlxButton;
	public var buttonUp:FlxButton;
	public var buttonRight:FlxButton;
	public var buttonDown:FlxButton;

	/**
	 * Group of directions buttons.
	 */
	public var dPad:FlxSpriteGroup;

	/**
	 * Group of action buttons.
	 */
	public var actions:FlxSpriteGroup;

	public function new()
	{
		super();
		scrollFactor.set();

		dPad = new FlxSpriteGroup();
		dPad.scrollFactor.set();

		actions = new FlxSpriteGroup();
        actions.scrollFactor.set();
        
        buttonUp = createButton(35, FlxG.height - 116, 44, 45, "up");
        buttonLeft = createButton(0, FlxG.height - 81, 44, 45, "left");
        buttonRight = createButton(69, FlxG.height - 81, 44, 45, "right");
        buttonDown = createButton(35, FlxG.height - 45, 44, 45, "down");
        // buttonB = createButton(FlxG.width - 86, FlxG.height - 45, 44, 45, "b")
		buttonA = createButton(FlxG.width - 44, FlxG.height - 45, 44, 45, "a");
		
		add(buttonA);
		add(buttonUp);
		add(buttonLeft);
		add(buttonRight);
		add(buttonDown);

		actions.add(buttonA);
		
		dPad.add(buttonUp);
		dPad.add(buttonLeft);
		dPad.add(buttonRight);
		dPad.add(buttonDown);
	}

	override public function destroy():Void
	{
		super.destroy();

		dPad = FlxDestroyUtil.destroy(dPad);
		actions = FlxDestroyUtil.destroy(actions);

		dPad = null;
		actions = null;
		buttonA = null;
		buttonB = null;
		
		buttonLeft = null;
		buttonUp = null;
		buttonDown = null;
		buttonRight = null;
	}

	/**
	 * @param   X          The x-position of the button.
	 * @param   Y          The y-position of the button.
	 * @param   Width      The width of the button.
	 * @param   Height     The height of the button.
	 * @param   Graphic    The image of the button. It must contains 3 frames (`NORMAL`, `HIGHLIGHT`, `PRESSED`).
	 * @param   Callback   The callback for the button.
	 * @return  The button
	 */
	public function createButton(X:Float, Y:Float, Width:Int, Height:Int, Graphic:String, ?OnClick:Void->Void):FlxButton
	{
		var button = new FlxButton(X, Y);
		// var frame = FlxAssets.getVirtualInputFrames().getByName(Graphic);
		// button.frames = FlxTileFrames.fromFrame(frame, FlxPoint.get(Width, Height));
		// button.resetSizeFromFrame();
		button.loadGraphic('assets/images/virtual-input-${Graphic}.png', true, Width, Height);
		button.solid = false;
		button.immovable = true;
		button.scrollFactor.set();

		#if FLX_DEBUG
		button.ignoreDrawDebug = true;
		#end

		if (OnClick != null)
			button.onDown.callback = OnClick;

		return button;
	}
}